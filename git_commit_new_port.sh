#!/bin/bash

export PORT_NAME="$1"
export PORTS_DIR="./ports"
export PORT_DIR="${PORTS_DIR}/${PORT_NAME}"
export JSON_MANAGER_PROGRAM=jq

if ! command -v ${JSON_MANAGER_PROGRAM} &> /dev/null; then
    echo " ${JSON_MANAGER_PROGRAM} could not be found"
    echo " - sudo apt install ${JSON_MANAGER_PROGRAM}"
    exit 1
fi

if [ ! -d ${PORT_DIR} ]; then
    echo " ${PORT_DIR} directory does not exist"
    exit 1
fi

if [ ! -f ${PORT_DIR}/"vcpkg.json" ]; then
    echo " ${PORT_DIR}/vcpkg.json file does not exist"
    exit 1
fi


function write_json_file(){
    # @args
    #  $1: JSON string
    #  $2: file dir
    cat <<<$(echo "$1" | ${JSON_MANAGER_PROGRAM} '.') > $2
}

# Get values 
VERSION=$(cat ${PORT_DIR}/"vcpkg.json" | ${JSON_MANAGER_PROGRAM} '.version')
PORT_VERSION=$(cat ${PORT_DIR}/"vcpkg.json" | ${JSON_MANAGER_PROGRAM} '."port-version"')

# Initize env variables 
export VERSION_DIR='./versions'
export VERSION_PORT_DIR="${VERSION_DIR}/${PORT_NAME:0:1}-"
export VERSION_PORT_FILE="${VERSION_PORT_DIR}/${PORT_NAME}.json"
export BASELINE_FILE="${VERSION_DIR}/baseline.json"

# Initialize JSON in versions folder if it does not exist
mkdir -p ${VERSION_PORT_DIR}

if [ ! -f ${VERSION_PORT_FILE} ]; then
    write_json_file '{"versions": []}' "${VERSION_PORT_FILE}"
    echo " ${VERSION_PORT_FILE} has been created"
fi

# Update baseline.json
BASE_LINE_JSON=$(${JSON_MANAGER_PROGRAM} --argjson version ${VERSION} --argjson port_version ${PORT_VERSION} --arg port_name ${PORT_NAME} '.default += {($port_name):{"baseline": $version, "port-version": $port_version}}' ${BASELINE_FILE})
write_json_file "${BASE_LINE_JSON}" "${BASELINE_FILE}"

# Git commit 
git add ${PORT_DIR}
git commit -m "[${PORT_NAME}] new version ${VERSION} and port #${PORT_VERSION}"

# Update version file with commit id
export COMMIT_ID=$(git rev-parse HEAD:${PORT_DIR})
VERSION_PORT_FILE_JSON=$(${JSON_MANAGER_PROGRAM} --argjson version ${VERSION} --argjson port_version ${PORT_VERSION} --arg commit_id $COMMIT_ID '.versions += [{"git-tree": $commit_id, "version": $version, "port-version": $port_version}]' ${VERSION_PORT_FILE})
write_json_file "${VERSION_PORT_FILE_JSON}" "${VERSION_PORT_FILE}"

# Edit previus commit
git add ${VERSION_DIR}
git commit --amend --no-edit