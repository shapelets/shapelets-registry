# Set the maximum number of parallel jobs during the build
if(DEFINED ENV{VCPKG_MAX_CONCURRENCY})
    set(VCPKG_CONCURRENCY $ENV{VCPKG_MAX_CONCURRENCY})
else()
    # Fallback to a default value if the environment variable is not set
    set(VCPKG_CONCURRENCY 8)
endif()

vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO duckdb/duckdb
    REF v1.1.1
    SHA512 8a268b3d4c44e7000fd74344571f538be91801f16fd36d0e7b3d9e186b7038d044adfc8b585e0ebe48dddd9a4f49f65f3480b30524aa00f80e1b9f4a10d0fc4c
    HEAD_REF master
)

FILE(COPY "${CMAKE_CURRENT_LIST_DIR}/extension_config_local.cmake" DESTINATION "${SOURCE_PATH}/extension")

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    OPTIONS
        -DOVERRIDE_GIT_DESCRIBE=v1.1.1                  #   Required for bulding with vcpkg
        -DBUILD_SHELL=OFF
        -DENABLE_EXTENSION_AUTOLOADING=ON               ##   Enable extension auto-loading by default                                                                                                    FALSE
        -DENABLE_EXTENSION_AUTOINSTAll=ON               ##   Enable extension auto-installing by default                                                                                                 FALSE
        -DENABLE_UBSAN=OFF                              ##   Enable undefined behavior sanitizer    
        -DENABLE_SANITIZER=OFF                          ##   Enable address sanitizer
        -DENABLE_THREAD_SANITIZER=OFF                   ##   Enable thread sanitizer
        -DDISABLE_VPTR_SANITIZER=ON                     ##   Disable vptr sanitizer; work-around for sanitizer false positive on Macbook M1                                                              FALSE
        -DEXTENSION_STATIC_BUILD=ON                     ##   Extension build linking statically with DuckDB. Required for building linux loadable extensions                                             FALSE
        -DBUILD_UNITTESTS=OFF                           ##   Build the C++ Unit Tests                                                                                                                    TRUE
        -DLOCAL_EXTENSION_REPO=""
        # -DVCPKG_MANIFEST_DIR="${SOURCE_PATH}"
        -DVCPKG_BUILD=1 
    MAYBE_UNUSED_VARIABLES    
)



# DISABLE_PARALLEL 
vcpkg_cmake_install()
vcpkg_copy_pdbs()

IF (WIN32)
    vcpkg_cmake_config_fixup(CONFIG_PATH cmake)
ELSE()
    vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/DuckDB)
ENDIF()


file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation/apis/v0/dev" 
                    "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation/apis/v0/v0.0" 
                    "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation/functions"
                    "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation/apis/v0"
                    "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation/apis"
                    "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation")

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/include/duckdb/storage/serialization")

# Handle copyright
configure_file(${SOURCE_PATH}/LICENSE ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)






# set(ENV{X_VCPKG_RECURSIVE_DATA} "{\"vcpkg-root-env\": \"/home/justo/tools/vcpkg\"}")


# execute_process(
#     COMMAND git clone https://github.com/microsoft/vcpkg.git build/vcpkg
#     WORKING_DIRECTORY ${SOURCE_PATH}
# )

# execute_process(
#     COMMAND bootstrap-vcpkg
#     WORKING_DIRECTORY ${SOURCE_PATH}/build/vcpkg
# )

# file(COPY ${CURRENT_PORT_DIR}/extension_config_local.cmake 
#      DESTINATION ${SOURCE_PATH}/extension)

# set(ENV{OVERRIDE_GIT_DESCRIBE} "v1.1.1-0-af39bd0dcf6")
# set(ENV{VCPKG_TOOLCHAIN_PATH} "/home/justo/tools/vcpkg/scripts/buildsystems/vcpkg.cmake")
# set(ENV{USE_MERGED_VCPKG_MANIFEST} 1)
# set(ENV{ENABLE_EXTENSION_AUTOLOADING} 1)
# set(ENV{ENABLE_EXTENSION_AUTOINSTALL} 1)
# set(ENV{DISABLE_UBSAN} 1)
# set(ENV{DISABLE_VPTR_SANITIZER} 1)
# set(ENV{EXTENSION_STATIC_BUILD} 1)
# set(VCPKG_MANIFEST_INSTALL ON)
# set(ENV{VCPKG_MANIFEST_INSTALL} "ON")

# set(ENV{COMMON_CMAKE_VARS} "-DBUILD_UNITTESTS=OFF -DCMAKE_INSTALL_PREFIX=${SOURCE_PATH}/build/install/debug")

# execute_process(
#     COMMAND make extension_configuration
#     WORKING_DIRECTORY ${SOURCE_PATH}
# )

# execute_process(
#     COMMAND make debug
#     WORKING_DIRECTORY ${SOURCE_PATH}
# )

# execute_process(
#     COMMAND cmake --build . --config debug --target install
#     WORKING_DIRECTORY ${SOURCE_PATH}/build/debug
# )

# set(ENV{COMMON_CMAKE_VARS} "-DBUILD_UNITTESTS=OFF -DCMAKE_INSTALL_PREFIX=${SOURCE_PATH}/build/install/release")

# execute_process(
#     COMMAND make release
#     WORKING_DIRECTORY ${SOURCE_PATH}
# )

# execute_process(
#     COMMAND cmake --build . --config debug --target install
#     WORKING_DIRECTORY ${SOURCE_PATH}/build/release
# )


# # # vcpkg_from_github(
# # #     OUT_SOURCE_PATH SOURCE_PATH
# # #     REPO duckdb/duckdb
# # #     REF fa5c2fe15f3da5f32397b009196c0895fce60820        #v1.1.0
# # #     SHA512 978667665b22821fc603bb689bea845dae27544793b52bc4a6c7ebf394efc5fb778573ae94f48168bcd571541b8491eb8710adcb79bc973d8b0f2409634ef8e0
# # #     HEAD_REF master
# # #     # PATCHES extensions.patch
# # # )

# # # set(ENV{OVERRIDE_GIT_DESCRIBE} "v1.1.0-0-gfa5c2fe15f")
# # # set(ENV{USE_MERGED_VCPKG_MANIFEST} 1)
# # # set(ENV{VCPKG_TOOLCHAIN_PATH} "$ENV{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake")
# # # set(ENV{ENABLE_EXTENSION_AUTOLOADING} 1)
# # # set(ENV{ENABLE_EXTENSION_AUTOINSTALL} 1)
# # # set(ENV{DISABLE_UBSAN} 1)
# # # set(ENV{DISABLE_VPTR_SANITIZER} 1)
# # # set(ENV{EXTENSION_STATIC_BUILD} 1)
# # # # set(ENV{BUILD_ALL_EXT} 1)
# # # set(ENV{EXTENSION_CONFIGS} "./extensions.cmake")
# # # set(ENV{COMMON_CMAKE_VARS} "-DBUILD_UNITTESTS=OFF -DCMAKE_INSTALL_PREFIX=${SOURCE_PATH}/build/INSTALL")

# # # execute_process(
# # #     COMMAND make extension_configuration 
# # #     WORKING_DIRECTORY ${SOURCE_PATH}
# # # )

# # # execute_process(
# # #     COMMAND make debug
# # #     WORKING_DIRECTORY ${SOURCE_PATH}
# # # )

# # # execute_process(
# # #     COMMAND cmake --build . --config debug --target install
# # #     WORKING_DIRECTORY ${SOURCE_PATH}/build/debug
# # # )

# # # file(COPY ${SOURCE_PATH}/build/INSTALL/include DESTINATION ${CURRENT_PACKAGES_DIR}/debug)
# # # file(COPY ${SOURCE_PATH}/build/INSTALL/lib DESTINATION ${CURRENT_PACKAGES_DIR}/debug)
# # # file(COPY ${SOURCE_PATH}/build/INSTALL/bin DESTINATION ${CURRENT_PACKAGES_DIR}/debug)
# # # file(REMOVE_RECURSE "${SOURCE_PATH}/build/INSTALL")

# # # execute_process(
# # #     COMMAND make release
# # #     WORKING_DIRECTORY ${SOURCE_PATH}
# # # )

# # # execute_process(
# # #     COMMAND cmake --build . --config release --target install
# # #     WORKING_DIRECTORY ${SOURCE_PATH}/build/release
# # # )

# # # file(COPY ${SOURCE_PATH}/build/INSTALL/include DESTINATION ${CURRENT_PACKAGES_DIR})
# # # file(COPY ${SOURCE_PATH}/build/INSTALL/lib DESTINATION ${CURRENT_PACKAGES_DIR})
# # # file(COPY ${SOURCE_PATH}/build/INSTALL/bin DESTINATION ${CURRENT_PACKAGES_DIR})

# # # # IF (WIN32)
# # # #     vcpkg_cmake_config_fixup(CONFIG_PATH cmake)
# # # # ELSE()
# # #     vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/DuckDB)
# # # # ENDIF()

# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")
# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/include/duckdb/main/capi/header_generation")
# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/include/duckdb/storage/serialization")

# # # configure_file(${SOURCE_PATH}/LICENSE ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)

# # # vcpkg_cmake_configure(
# # #     SOURCE_PATH ${SOURCE_PATH}
# # #     OPTIONS
# # #         -DOVERRIDE_GIT_DESCRIBE=v1.1.0-0-gfa5c2fe15f          #   Required for bulding with vcpkg
# # #         -DDISABLE_UNITY=OFF                     #   Disable unity builds                                                                                                                        FALSE
# # #         -DUSE_WASM_THREADS=OFF                  #   Should threads be used                                                                                                                      FALSE
# # #         -DFORCE_COLORED_OUTPUT=OFF              #   Always produce ANSI-colored output (GNU/Clang only).                                                                                        FALSE
# # #         -DFORCE_WARN_UNUSED=OFF                 #   Unused code objects lead to compiler warnings                                                                                               FALSE
# # #         -DENABLE_EXTENSION_AUTOLOADING=ON      ##   Enable extension auto-loading by default                                                                                                    FALSE
# # #         -DENABLE_EXTENSION_AUTOINSTAll=ON      ##   Enable extension auto-installing by default                                                                                                 FALSE
# # #         -DEXTENSION_TESTS_ONLY=OFF              #   Only load the tests for extensions, don't actually build them; useful for testing loadable extensions                                       FALSE
# # #         -DWASM_LOADABLE_EXTENSIONS=OFF          #   WebAssembly build with loadable extensions                                                                                                  FALSE
# # #         -DENABLE_SANITIZER=OFF                  #   Enable address sanitizer                                                                                                                    FALSE
# # #         -DENABLE_THREAD_SANITIZER=OFF           #   Enable thread sanitizer                                                                                                                     FALSE
# # #         -DENABLE_UBSAN=OFF                     ##   Enable undefined behavior sanitizer                                                                                                         TRUE
# # #         -DDISABLE_VPTR_SANITIZER=ON            ##   Disable vptr sanitizer; work-around for sanitizer false positive on Macbook M1                                                              FALSE
# # #         -DFORCE_SANITIZER=OFF                   #   Forces building with sanitizers even if the Python and R modules are enabled                                                                FALSE
# # #         -DEXPLICIT_EXCEPTIONS=OFF               #   Explicitly enable C++ exceptions                                                                                                            FALSE
# # #         -DOSX_BUILD_UNIVERSAL=OFF               #   Build both architectures on OSX and create a single binary containing both                                                                  FALSE
# # #         -DAMALGAMATION_BUILD=OFF                #   Build from the amalgamation files, rather than from the normal sources.                                                                     FALSE
# # #         -DBUILD_MAIN_DUCKDB_LIBRARY=ON          #   Build the main duckdb library and executable                                                                                                TRUE
# # #         -DEXTENSION_STATIC_BUILD=ON            ##   Extension build linking statically with DuckDB. Required for building linux loadable extensions                                             FALSE
# # #         -DBUILD_EXTENSIONS_ONLY=OFF             #   Build all extension as linkable, overriding DONT_LINK, and don't build core.                                                                FALSE
# # #         -DBUILD_CORE_FUNCTIONS_EXTENSION=ON     #   Build the core functions                                                                                                                    TRUE
# # #         -DBUILD_BENCHMARKS=OFF                  #   Enable building of the benchmark suite                                                                                                      FALSE
# # #         -DBUILD_TPCE=OFF                        #   Enable building of the TPC-E tool                                                                                                           FALSE
# # #         -DDISABLE_BUILTIN_EXTENSIONS=OFF        #   Disable linking extensions                                                                                                                  FALSE
# # #         -DBUILD_PYTHON=OFF                      #   Build the DuckDB Python extension                                                                                                           FALSE 
# # #         -DUSER_SPACE=OFF                        #   Build the DuckDB Python in the user space                                                                                                   FALSE
# # #         -DFORCE_QUERY_LOG=OFF                   #   If enabled, all queries will be logged to the specified path                                                                                FALSE
# # #         -DBUILD_SHELL=OFF                       #   Build the DuckDB Shell and SQLite API Wrappers                                                                                              FALSE
# # #         -DDISABLE_THREADS=OFF                   #   Disable support for multi-threading                                                                                                         FALSE
# # #         -DDISABLE_EXTENSION_LOAD=OFF            #   Disable support for loading and installing extensions                                                                                       FALSE
# # #         -DDISABLE_STR_INLINE=OFF                #   Debug setting: disable inlining of strings                                                                                                  FALSE
# # #         -DDISABLE_MEMORY_SAFETY=OFF             #   Debug setting: disable memory access checks at runtime                                                                                      FALSE
# # #         -DDISABLE_ASSERTIONS=OFF                #   Debug setting: disable assertions                                                                                                           FALSE
# # #         -DALTERNATIVE_VERIFY=OFF                #   Debug setting: use alternative verify mode                                                                                                  FALSE
# # #         -DRUN_SLOW_VERIFIERS=OFF                #   Debug setting: enable a more extensive set of verifiers                                                                                     FALSE
# # #         -DDESTROY_UNPINNED_BLOCKS=OFF           #   Debug setting: destroy unpinned buffer-managed blocks                                                                                       FALSE
# # #         -DFORCE_ASYNC_SINK_SOURCE=OFF           #   Debug setting: forces sinks/sources to block the first 2 times they're called                                                               FALSE
# # #         -DDEBUG_ALLOCATION=OFF                  #   Debug setting: keep track of outstanding allocations to detect memory leaks                                                                 FALSE
# # #         -DDEBUG_STACKTRACE=OFF                  #   Debug setting: print a stracktrace on asserts and when testing crashes                                                                      FALSE
# # #         -DDEBUG_MOVE=OFF                        #   Debug setting: Ensure std::move is being used                                                                                               FALSE
# # #         -DVERIFY_VECTOR=none                    #   Debug setting: verify vectors (options: dictionary_expression, dictionary_operator, constant_operator, sequence_operator, nested_shuffle)   "none"
# # #         -DCLANG_TIDY=OFF                        #   Enable build for clang-tidy, this disables all source files excluding the core database. This does not produce a working build.             FALSE
# # #         -DBUILD_UNITTESTS=OFF                  ##   Build the C++ Unit Tests                                                                                                                    TRUE
# # #         -DEXTENSION_CONFIG_BUILD=OFF            #   Produce extension configuration artifacts instead of building. (such as shared vcpkg.json, extensions.txt)                                  FLASE
# # #         -DCUSTOM_LINKER=                        #   Use a custom linker program                                                                                                                 ""
# # #         -DASSERT_EXCEPTION=ON                   #   Throw an exception on an assert failing, instead of triggering a sigabort                                                                   TRUE
# # #         -DFORCE_ASSERT=OFF                      #   Enable checking of assertions, even in release mode                                                                                         FALSE
# # #         -DTREAT_WARNINGS_AS_ERRORS=OFF          #   Treat warnings as errors                                                                                                                    FALSE
# # #         -DEXPORT_DLL_SYMBOLS=ON                 #   Export dll symbols on Windows, else import                                                                                                  TRUE
# # #         -DBUILD_RDTSC=OFF                       #   Enable the rdtsc instruction                                                                                                                FALSE
# # #         -DTEST_REMOTE_INSTALL=OFF               #   Test installation of specific extensions                                                                                                    FALSE
# # #         -DLOCAL_EXTENSION_REPO=""
# # #         -DVCPKG_MANIFEST_DIR="${SOURCE_PATH}"
# # #         -DVCPKG_BUILD=1 
# # #     MAYBE_UNUSED_VARIABLES    
# # # )

# # # # DISABLE_PARALLEL 
# # # vcpkg_cmake_install()
# # # vcpkg_copy_pdbs()



# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")
# # # file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/include/duckdb/storage/serialization")

# # # # Handle copyright
# # # configure_file(${SOURCE_PATH}/LICENSE ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)