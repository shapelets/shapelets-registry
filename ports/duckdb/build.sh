#rm -rf duckdb
#git clone https://github.com/duckdb/duckdb.git
cd duckdb
#git switch --detach v1.1.0
#cp ../extension_config_local.cmake extension/extension_config_local.cmake

set_env_vars() {
    # export OVERRIDE_GIT_DESCRIBE="v1.1.0"
    export USE_MERGED_VCPKG_MANIFEST=1
    export VCPKG_TOOLCHAIN_PATH="${VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake"
    export ENABLE_EXTENSION_AUTOLOADING=1
    export ENABLE_EXTENSION_AUTOINSTALL=1
    export DISABLE_UBSAN=1
    export DISABLE_VPTR_SANITIZER=1
    export EXTENSION_STATIC_BUILD=1
    export COMMON_CMAKE_VARS="-DBUILD_UNITTESTS=OFF -DCMAKE_INSTALL_PREFIX=../../../$1"
}

rm -rf build
set_env_vars debug && make extension_configuration
set_env_vars debug && make debug
cd build/debug
set_env_vars debug && cmake --build . --config debug --target install
cd ../..
set_env_vars release && make release
cd build/release
set_env_vars release && cmake --build . --config release --target install
