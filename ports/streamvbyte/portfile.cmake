# header-only library
vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO lemire/streamvbyte
    REF a27311ccfc2d4277e9ba1f67a13f6136bc2796ba
    SHA512 05165b74576d15fd43d58001b2fc9e62d58603b9122e857ae3d419c5b0b2d2e6d8d3f98d450d116493e608e7a400b48b351d3199a8a8feef27efc07b8a57b074
    HEAD_REF master
    PATCHES install.patch
)

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    DISABLE_PARALLEL_CONFIGURE
    OPTIONS
        -DSTREAMVBYTE_SANITIZE=OFF
        -DSTREAMVBYTE_SANITIZE_UNDEFINED=OFF
        -DSTREAMVBYTE_ENABLE_TESTS=OFF
)

vcpkg_cmake_install()

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")

# Handle copyright
configure_file(${SOURCE_PATH}/LICENSE ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)