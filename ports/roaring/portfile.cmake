
vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO shapelets/CRoaring
    REF 3ef7b44a4f74af09fe19bb84d30bc35cab130561
    SHA512 3a0342d90f16df7f8a707fca2df7d4491b762b64a1abbf822a14e3dcd8e03e35930a0534e25ee36463898ab6cae1a05e089af6dce2ddba1513ca9c2c1befb123
    HEAD_REF master
)

set(VCPKG_LIBRARY_LINKAGE static)

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    DISABLE_PARALLEL_CONFIGURE
    OPTIONS
        -DROARING_BUILD_STATIC=ON
        -DENABLE_ROARING_TESTS=OFF
)

vcpkg_cmake_install()

vcpkg_copy_pdbs()

vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/${PORT})

file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/include)

vcpkg_fixup_pkgconfig()

# Handle copyright
configure_file(${SOURCE_PATH}/LICENSE ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)