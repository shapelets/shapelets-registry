vcpkg_from_git(
    OUT_SOURCE_PATH SOURCE_PATH
    URL git@bitbucket.org:shapelets/disruptor.git
    REF 0deda23eb8cf8cac8dc41aa7ad4e6fe55eac1825
    HEAD_REF master
)

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    OPTIONS
        -DBUILD_TESTS=OFF
        -DENABLE_AVX2=ON
)

vcpkg_cmake_install()

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/share")

vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/shapelets-disruptor)

# Handle copyright
configure_file(${SOURCE_PATH}/LICENCE.txt ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)