# header-only library
vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO fastfloat/fast_float
    REF e92c63295c64de6423398a915c3fd262b8d07f9d
    SHA512 476daf58d2d1f33555567fa784ac4614b9a0f923f511cce68de61d2e518f76e3963a6725746d4a47e808c311c657880ffbf6d078159592db9d6cd8d71ab4e57c
    HEAD_REF master
)

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    DISABLE_PARALLEL_CONFIGURE
    OPTIONS
        -DFASTFLOAT_TEST=OFF
        -DFASTFLOAT_SANITIZE=OFF
        -DFASTFLOAT_INSTALL=ON
)

vcpkg_cmake_install()

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug")

# Handle copyright
configure_file(${SOURCE_PATH}/LICENSE-MIT ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)