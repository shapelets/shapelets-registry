
vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO LLNL/units
    REF eb8d59bf1a6a82c3087b4448c450252ea1e67873
    SHA512 61ca3ca8ff4f9860c8788796fa6eb4f833c1a22b1454791b2263f640f322365bb456988d4a8c37e49034d938d6e1c1253191d2d9e177f6646e0fb2d6a66f8ffc
    HEAD_REF master
)
set(VCPKG_LIBRARY_LINKAGE static)

# UNITS_ENABLE_TESTS is left ON to ensure it compiles with 
# the option ENABLE_UNIT_MAP_ACCESS
vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    DISABLE_PARALLEL_CONFIGURE
    OPTIONS
        -DUNITS_ENABLE_TESTS=OFF
        -DUNITS_INSTALL=ON
        -DUNITS_BUILD_CONVERTER_APP=OFF
        -DUNITS_BUILD_WEBSERVER=OFF
)

vcpkg_cmake_install()
vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/units)

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/bin")
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/bin")

# Handle copyright
configure_file(${SOURCE_PATH}/LICENSE ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)