message(WARNING "EVE requires a C++ 20 compliant compiler. GCC-11 and clang-12 are known to work.")

vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO jfalcou/eve
    REF afd2707cd8dc434281f1b1d9b1f8b0256808a92c
    SHA512 d50ec69913062afc93318a3ea0191bc74bde348615f470a05b7e9647a3cfc74f0b471594c375fb60e7b94dd4b4a542b58f4c38220c6386c198ad92680c248880
    PATCHES fix_install_path.patch
    HEAD_REF develop
)

vcpkg_cmake_configure(SOURCE_PATH "${SOURCE_PATH}")

vcpkg_cmake_install()

file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug")
file(INSTALL "${SOURCE_PATH}/LICENSE.md" DESTINATION "${CURRENT_PACKAGES_DIR}/share/eve" RENAME copyright)