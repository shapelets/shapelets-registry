
vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO HdrHistogram/HdrHistogram_c
    REF da51af3cec4414f47558c83e4f00f4f8216e0a96
    SHA512 3b86253df154f013a916898b8c219272e0dfaadfed056e91d6428b20003eec018736dfd5aefb25ccc26c31cdf1b01df178861e1edc60ae401c614c01641e6b8f
    HEAD_REF master
    PATCHES change_project_name.patch
)

set(VCPKG_LIBRARY_LINKAGE static)

vcpkg_cmake_configure(
    SOURCE_PATH ${SOURCE_PATH}
    DISABLE_PARALLEL_CONFIGURE
    OPTIONS
        -DHDR_HISTOGRAM_BUILD_PROGRAMS=OFF
        -DHDR_LOG_REQUIRED=ON
)

vcpkg_cmake_install()

vcpkg_copy_pdbs()

vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/hdrhistogram)

file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/include)

vcpkg_fixup_pkgconfig()

# Handle copyright
configure_file(${SOURCE_PATH}/LICENSE.txt ${CURRENT_PACKAGES_DIR}/share/${PORT}/copyright COPYONLY)