# Shapelets-registry
  Custom vcpkg registry with the private shapelets projects.

## Commit a new version and/or port
First of all, the new version and port must be validated with the following commands:
```
$ vcpkg remove PKG_NAME
$ vcpkg install PKG_NAME --overlay-ports=ports/PKG_NAME
```
In case the package was compiled without errors, create a git commit of the new changes and the port tree, using the command
```
$ ./git_commit_new_port.sh PKG_NAME
```
This script will update the files in `versions` folder and create the git commit. 
### Git commit manually
 In case of preferring to do this process manually, the following steps must be performed to create a git commit:  
```
$ git add ports/PKG_NAME
$ git commit -m "[PKG_NAME] new version ${VERSION} and port #${PORT_VERSION}"
$ git rev-parse HEAD:ports/PKG_NAME
```
 Add git-tree, version and port in `versions/../PKG_NAME.json`, and update baseline and port in `versions/baseline.json`
```
$ git add versions
$ git commit --amend --no-edit
```
Finally, a git push can be executed.

## References

1. https://devblogs.microsoft.com/cppblog/registries-bring-your-own-libraries-to-vcpkg/